**Jumia Test by Guilherme Costa**

https://hub.docker.com/repository/docker/guigas66/jumia_test

docker push guigas66/jumia_test:v6

docker run -d --rm -p 8080:8080 jumiatest-web:v6

http://localhost:8080/jumiaTest

**Run project outside docker**

Spring Boot project please run: 

mvn clean package install
mvn spring-boot:run


