'use strict';

angular.module('jumiaTest').controller('CustomerController',
    ['CustomerService', '$scope', function(CustomerService, $scope) {

        var self = this;
       
        self.getAllCustomer = getAllCustomer;
      
        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function getAllCustomer(){
            return CustomerService.getAllCustomer();
            console.log('Fetching all customers');
        }
        
       
    }

    ]);