var app = angular.module('jumiaTest',['ui.router','ngStorage', 'angularUtils.directives.dirPagination']);
 
app.constant('urls', {
    BASE: 'http://localhost:8080/jumiaTest',
    USER_SERVICE_API : 'http://localhost:8080/jumiaTest/rest/customer/all'
});
 
app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
 
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'rest/customer/',
                controller:'CustomerController',
                controllerAs:'ctrl',
                resolve: {
                    users: function ($q, CustomerService) {
                        console.log('Load all customers');
                        var deferred = $q.defer();
                        CustomerService.listAllCustomers().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);