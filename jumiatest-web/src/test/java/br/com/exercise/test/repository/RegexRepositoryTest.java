package br.com.exercise.test.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.exercise.entity.RegexPhone;
import br.com.exercise.repository.RegexRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegexRepositoryTest {

	@Autowired
	private RegexRepository regexRepository;
	
	
    @Test
	public void testFindAll() {
		
		List<RegexPhone> list = (List<RegexPhone>) regexRepository.findAll();
		
		assertTrue(list.size() == 240);
	}
}
