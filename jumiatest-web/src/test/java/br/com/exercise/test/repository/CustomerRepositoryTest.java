package br.com.exercise.test.repository;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.exercise.entity.Customer;
import br.com.exercise.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTest {

	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Mock
	private CustomerRepository customerRepositoryMock;

	
    @Test
	public void testFindAll() {
		
		List<Customer> list = (List<Customer>) customerRepository.findAll();
		
		assertTrue(list.size() > 1);
	}
	
	
}
