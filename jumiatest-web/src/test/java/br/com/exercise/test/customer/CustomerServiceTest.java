package br.com.exercise.test.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.exercise.dto.CustomerDTO;
import br.com.exercise.entity.Customer;
import br.com.exercise.exception.CustomerServiceException;
import br.com.exercise.repository.CustomerRepository;
import br.com.exercise.repository.RegexRepository;
import br.com.exercise.service.CustomerService;
import br.com.exercise.util.ISOCountryCodeEnum;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

	@Autowired
	private CustomerService customerServices;
	
	@Mock
	private CustomerRepository customerRepositoryMock;
	
	@Mock
	private RegexRepository regexRepositoryMock;
	
	
    @Test
	public void testFindAll() {
		List<CustomerDTO> list = customerServices.listAllCustomers();
		assertTrue(list.size() > 1);
	}
    
    /**
     * Morocco
     */
    
    @Test
	public void testIdentifyPhoneMorocco() {
    		
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Chouf Malo");
    		customer.setPhone("(212) 691933626");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.MA,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneMoroccoInvalidRegex() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Chouf Malo");
    		customer.setPhone("(212) 69193362634");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneMoroccoInvalidCodePhone() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Chouf Malo");
    		customer.setPhone("(210) 691933626");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    /**
     * Ethiopia
     */
    
    @Test
	public void testIdentifyPhoneEthiopia() {
    		
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Filimon Embaye");
    		customer.setPhone("(251) 914701723");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.ET,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneEthiopiaInvalidRegex() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Filimon Embaye");
    		customer.setPhone("(251) 9147017231");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneEthiopiaInvalidCodePhone() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Filimon Embaye");
    		customer.setPhone("(253) 914701723");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    /**
     * Cameroon
     */
    
    @Test
	public void testIdentifyPhoneCameroon() {
    		
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("EMILE CHRISTIAN KOUKOU DIKANDA HONORE");
    		customer.setPhone("(237) 697151594");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.CM,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneCameroonInvalidRegex() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("EMILE CHRISTIAN KOUKOU DIKANDA HONORE");
    		customer.setPhone("(237) 69715159433");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneCameroonInvalidCodePhone() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("EMILE CHRISTIAN KOUKOU DIKANDA HONORE");
    		customer.setPhone("(251) 697151594");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    /**
     * Mozambique
     */
    
    @Test
	public void testIdentifyPhoneMozambique() {
    		
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Edunildo Gomes Alberto");
    		customer.setPhone("(258) 847651504");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.MZ, retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneMozambiqueInvalidRegex() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Edunildo Gomes Alberto");
    		customer.setPhone("(258) 84765150422");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneMozambiqueInvalidCodePhone() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Edunildo Gomes Alberto");
    		customer.setPhone("(252) 84765150422");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    /**
     * Uganda
     */
    
    @Test
	public void testIdentifyPhoneUganda() {
    		
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Daniel Makori");
    		customer.setPhone("(256) 714660221");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UG, retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneUgandaInvalidRegex() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Daniel Makori");
    		customer.setPhone("(256) 7146602212");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    @Test
	public void testIdentifyPhoneUgandaInvalidCodePhone() {
    
    		Customer customer = new Customer();
    		customer.setId(1);
    		customer.setName("Daniel Makori");
    		customer.setPhone("(222) 714660221");
    		
    		ISOCountryCodeEnum retorno = customerServices.indentifyPhone(customer.getPhone());
    		
    		assertEquals(ISOCountryCodeEnum.UNDEFINED,retorno);
		
	}
    
    
    @Test(expected=CustomerServiceException.class)
	public void testIdentifyPhoneException() {
   
    		customerServices.indentifyPhone(null);    		
		
	}

}
