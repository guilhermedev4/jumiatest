<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">The Exercise</span></div>
        <div class="panel-body">
        
        <label>Create a single page application in Java (Frameworks allowed) that uses the provided database (SQLite 3) to list and categorize country phone numbers.
Phone numbers should be categorized by country, state (valid or not valid), country code and number.
The page should render a list of all phone numbers available in the DB. It should be possible to filter by country and state. Pagination is an extra.</label>
            
        </div>    
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">Customer List</span></div>
        <div class="panel-body">
            <div class="table-responsive">
            
               <table class="table table-striped table-hover"> 
               
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th><a href="#" ng-click="sortType = 'isoCodeCountry'; sortReverse = !sortReverse">COUNTRY 
            <span ng-show="sortType == 'isoCodeCountry' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'isoCodeCountry' && sortReverse" class="fa fa-caret-up"></span></a>
                        </th>
                        <th><a href="#" ng-click="sortType = 'isValid'; sortReverse = !sortReverse">PHONE IS VALID 
            <span ng-show="sortType == 'isValid' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'isValid' && sortReverse" class="fa fa-caret-up"></span></a>
                        (True/False)</th>
                        <th><a href="#" ng-click="sortType = 'codePhone'; sortReverse = !sortReverse">CODE PHONE 
            <span ng-show="sortType == 'codePhone' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'codePhone' && sortReverse" class="fa fa-caret-up"></span></a>
                        </th>
                        <th>PHONE</th>
                    </tr>
                    </thead>
                    <tbody>
                   <tr>
                    		<td></td>
                    		<td><input ng-model="search.name"/></td>
                    		<td><input ng-model="search.isoCodeCountry"/></td>
                    		<td><input ng-model="search.isValid"/></td>
                    		<td><input ng-model="search.codePhone"/></td>
                    		<td><input ng-model="search.phone"/></td>
                    </tr> 
                    	<tr dir-paginate="u in ctrl.getAllCustomer() | orderBy:sortType:sortReverse|filter:search|itemsPerPage:10" > 
                        <td>{{u.id}}</td>
                        <td>{{u.name}}</td>
                        <td>{{u.isoCodeCountry}}</td>
                        <td><img ng-src="{{u.isValid ? 'images/yes.png' :'images/no.png'}}" width=20 height=20 ></td>
                        <td>{{u.codePhone}}</td>
                        <td>{{u.phone}}</td>
                    </tr>
                    
                </table>    
                <dir-pagination-controls
				        max-size="5"
				        direction-links="true"
				        boundary-links="true" >
				    </dir-pagination-controls>
                    </tbody>  
            </div>
        </div>
    </div>
</div>