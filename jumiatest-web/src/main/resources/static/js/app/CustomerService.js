'use strict';
 
angular.module('jumiaTest').factory('CustomerService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {
 
            var factory = {
            		listAllCustomers: listAllCustomers,
            		getAllCustomer: getAllCustomer
            };
 
            return factory;
 
            function listAllCustomers() {
                console.log('Fetching all customers');
                var deferred = $q.defer();
                $http.get(urls.USER_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all customers');
                            $localStorage.customers = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading customers');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function getAllCustomer(){
                return $localStorage.customers;
            }
            
            
          
 
        }
    ]);