package br.com.exercise.util;


/**
 * Custom Erro by not usage
 * 
 * @author guilhermefreitas
 *
 */
public class CustomErrorType {
	 
    private String errorMessage;
 
    public CustomErrorType(String errorMessage){
        this.errorMessage = errorMessage;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }
 
}
