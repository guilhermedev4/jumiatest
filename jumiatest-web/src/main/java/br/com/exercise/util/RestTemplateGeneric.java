package br.com.exercise.util;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.http.HttpHeaders;

/**
 * Rest template for return in API Rest Services
 * 
 * @author guilhermefreitas
 *
 * @param <T>
 */
public class RestTemplateGeneric<T extends Serializable>{

	
	public static HttpHeaders buildHeaderRetorno(String cod, String msg) {
		HttpHeaders h = new HttpHeaders();
		ArrayList<String> msgs = new ArrayList<String>();
		msgs.add(cod);
		msgs.add(msg);
		h.put(HEADER_RETORNO, msgs);
		return h;
	}
	
	public static final String HEADER_RETORNO = "headerAdicMessage";
}