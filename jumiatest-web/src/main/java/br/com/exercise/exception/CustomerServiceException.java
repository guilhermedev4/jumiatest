package br.com.exercise.exception;

/**
 * Exception for all service interactions 
 * 
 * @author guilhermefreitas
 *
 */
public class CustomerServiceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CustomerServiceException(String message) {
        super("Error in service registered phones "+message);
    }
	
	

}
