package br.com.exercise.exception;


/**
 * Exception for all database interactions 
 * 
 * @author guilhermefreitas
 *
 */
public class CustomerRepositoryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CustomerRepositoryException(String message) {
        super("Error accessing customer data "+message);
    }
	
	

}
