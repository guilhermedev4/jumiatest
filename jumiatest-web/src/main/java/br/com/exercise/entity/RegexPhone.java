package br.com.exercise.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Object relational database for entity - RegexPhone
 * 
 * Register the regex of all countries in the world 
 * 
 * To work simply include the regex in the ISO code two letters of the corresponding country
 * 
 * @author guilhermefreitas
 *
 */
@Entity
@Table(name="regex_phone")
public class RegexPhone {
	
	
	/**
	 * ISO 2 Letters - standard international
	 */
	@Id
	@Column
	public String codeisocountry;
	
	@Column
	public String regex;

	
	public String getCodeisocountry() {
		return codeisocountry;
	}

	public void setCodeisocountry(String codeisocountry) {
		this.codeisocountry = codeisocountry;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}
	
	
	

}
