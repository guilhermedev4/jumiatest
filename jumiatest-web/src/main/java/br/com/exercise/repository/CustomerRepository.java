package br.com.exercise.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.exercise.entity.Customer;


/**
 * Interface exclusive for Customer 
 * 
 * @author guilhermefreitas
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
