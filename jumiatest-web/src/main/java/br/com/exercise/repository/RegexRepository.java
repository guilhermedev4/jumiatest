package br.com.exercise.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.exercise.entity.RegexPhone;

/**
 * Interface exclusive for Regex Repository
 * 
 * @author guilhermefreitas
 *
 */
public interface RegexRepository extends CrudRepository<RegexPhone, String> {

}
