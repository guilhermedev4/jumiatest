package br.com.exercise.dto;

import java.io.Serializable;

import br.com.exercise.util.ISOCountryCodeEnum;

/**
 * 
 * Data transfer object for Customer
 * 
 * @author guilhermefreitas
 *
 */
public class CustomerDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String name;
	
	private String phone;
	
	private Boolean isValid;	
	
	private String codePhone;
	
	private ISOCountryCodeEnum isoCodeCountry;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public ISOCountryCodeEnum getIsoCodeCountry() {
		return isoCodeCountry;
	}


	public void setIsoCodeCountry(ISOCountryCodeEnum isoCodeCountry) {
		this.isoCodeCountry = isoCodeCountry;
	}


	public Boolean getIsValid() {
		return isValid;
	}


	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}


	public String getCodePhone() {
		return codePhone;
	}


	public void setCodePhone(String codePhone) {
		this.codePhone = codePhone;
	}
	
	

}
