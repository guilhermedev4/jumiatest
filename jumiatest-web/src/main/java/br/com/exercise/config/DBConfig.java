package br.com.exercise.config;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Data base Configurator 
 * SQLITE (embedded database)
 * Local datastore in project main/resources
 * 
 * @author guilhermefreitas
 *
 */
@Configuration
public class DBConfig {
	
	/**
	 * Main datasource
	 * 
	 * @return javax.sql.DataSource.class 
	 */
    @Bean
	public DataSource dataSource() {
	        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
	        dataSourceBuilder.driverClassName("org.sqlite.JDBC");
	        dataSourceBuilder.url("jdbc:sqlite::resource:sample.db");
	        return dataSourceBuilder.build();   
	}
}
