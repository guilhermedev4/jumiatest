package br.com.exercise.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.exercise.dto.CustomerDTO;
import br.com.exercise.entity.Customer;
import br.com.exercise.entity.RegexPhone;
import br.com.exercise.exception.CustomerServiceException;
import br.com.exercise.repository.CustomerRepository;
import br.com.exercise.repository.RegexRepository;
import br.com.exercise.util.ISOCountryCodeEnum;

/**
 * Main service for Customer in application
 * 
 * @author guilhermefreitas
 *
 */
@Service
public class CustomerService {

	private static Logger logger = LogManager.getLogger(CustomerService.class);

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private RegexRepository regexRepository;

	private List<RegexPhone> listRegex = new ArrayList<RegexPhone>();
	
	/**
	 * Load all regex for working in the application
	 */
	@PostConstruct
	private void loadRegex() {
		try {

			for (RegexPhone u : regexRepository.findAll()) {

				listRegex.add(u);
			}

		} catch (Exception e) {
			logger.error("Load Regex Error " + e.getMessage());
			throw new CustomerServiceException("Load Regex Error " + e.getMessage());
		}
	}

	
	/**
	 * List all customers
	 *  
	 * @return br.com.exercise.dto.CustomerDTO
	 * @throws CustomerServiceException
	 */
	public List<CustomerDTO> listAllCustomers() throws CustomerServiceException {

		try {

			List<CustomerDTO> listDTOReturn = new ArrayList<CustomerDTO>();
			for (Customer client : customerRepository.findAll()) {
				CustomerDTO dto = new CustomerDTO();
				dto.setId(client.getId());
				dto.setName(client.getName());
				dto.setPhone(client.getPhone());
				dto.setIsoCodeCountry(indentifyPhone(client.getPhone()));
				if(dto.getIsoCodeCountry().equals(ISOCountryCodeEnum.UNDEFINED)) {
					dto.setIsValid(false);
				}else {
					dto.setIsValid(true);
				}
				dto.setCodePhone(dto.getIsoCodeCountry().getCountryCodeNumber());
				listDTOReturn.add(dto);
			}

			return listDTOReturn;

		} catch (Exception e) {
			logger.error("Listing Service Error " + e.getMessage());
			throw new CustomerServiceException(e.getMessage());
		}
	}

	/**
	 * Indentify Phone and Code International Phone prefix
	 * 
	 * @param phone
	 * @return br.com.exercise.util.ISOCountryCodeEnum
	 */
	public ISOCountryCodeEnum indentifyPhone(String phone) {

		try {

			for (RegexPhone regex : listRegex) {

				if (phone.matches(regex.regex)) {

					for (ISOCountryCodeEnum value : ISOCountryCodeEnum.values()) {

						if (phone.contains("(" + value.getCountryCodeNumber() + ")")) {
							return value;
						}
					}

				}

			}

			return ISOCountryCodeEnum.UNDEFINED;

		} catch (Exception e) {
			logger.error("Error identifying phone source " + e.getMessage());
			throw new CustomerServiceException(e.getMessage());
		}

	}

}
