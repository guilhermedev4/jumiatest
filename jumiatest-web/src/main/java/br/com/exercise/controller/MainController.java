package br.com.exercise.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * Index Controller
 * 
 * @author guilhermefreitas
 *
 */
@RestController
@RequestMapping("/")
public class MainController {
	
	
	/**
	 * Main redirection
	 * 
	 * @param modal
	 * @return
	 */
	@RequestMapping("/")
    public ModelAndView home(ModelMap modal) {
		ModelAndView modelView = new ModelAndView();
		modelView.addObject("title", "Jumia Test - Guilherme Costa");
		modelView.setViewName("index");
        return modelView;
    }

}
