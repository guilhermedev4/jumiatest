package br.com.exercise.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import br.com.exercise.dto.CustomerDTO;
import br.com.exercise.service.CustomerService;
import br.com.exercise.util.RestTemplateGeneric;

/**
 * Customer Controller 
 * 
 * API Services for project
 * 
 * @author guilhermefreitas
 *
 */
@RestController
@RequestMapping("/rest/customer")
public class CustomerController {
	
	private static Logger logger = LogManager.getLogger(CustomerController.class);


	private CustomerService customerServices;

	public CustomerController(CustomerService customerServices) {
		this.customerServices = customerServices;
	}

	/**
	 * 
	 * List All Customer
	 * 
	 * @return br.com.exercise.dto.CustomerDTO
	 * @throws Exception
	 */
	@CrossOrigin(origins = "*")
	@GetMapping("/all")
	public ResponseEntity<List<CustomerDTO>> listar() throws Exception {
		ResponseEntity<List<CustomerDTO>> response = null;
		
		
		try {
			logger.info("Received a query request");
			response = new ResponseEntity<List<CustomerDTO>>(customerServices.listAllCustomers(),
					RestTemplateGeneric.buildHeaderRetorno("200", "Successfully listed customers "), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error in consulting Customers");
			response = new ResponseEntity<List<CustomerDTO>>(new ArrayList<CustomerDTO>(),
					RestTemplateGeneric.buildHeaderRetorno("000", e.getMessage()), HttpStatus.OK);
		}
		return response;
	}

	/**
	 * 
	 * Redirect pages for FreeMaker
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping("/{page}")
	public ModelAndView partialHandler(@PathVariable("page") final String page) {
		ModelAndView modelView = new ModelAndView();
		modelView.setViewName(page);
		return modelView;
	}
	
	/**
	 * Redirect for list page.
	 * 
	 * @param modal
	 * @return
	 */
	@RequestMapping("/")
    public ModelAndView home(ModelMap modal) {
		ModelAndView modelView = new ModelAndView();
		modelView.setViewName("list");
        return modelView;
    }

}
